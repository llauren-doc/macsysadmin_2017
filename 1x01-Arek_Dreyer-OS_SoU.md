Day 1, set 1

# Arek Dreyer: OS State of the Union

_"My perspective is shaped by my experience"_

* The community continues to grow
* Use the community and Apple resources
* Share your knowledge and experience* Apple continues to innovate
* You need to continue to learn

Readme: [macOS Technical Training Guide](https://training.apple.com)

## Good blogs

* DerFlounder
* Managing Mac OS X
* AFP 548
* krypted.com
* ScriptingosX.com

## User groups

* MacBrained.org

## Podcasts

* MacAdmins Podcast

## Misc Open Source stuff

* Netflix Tech blog ("Stethoscope")

## What if i have 19, not 90k Macs

* Imagr
* Airwatch
* DEP Notify
* AutoPKGR
* CreateUSRpackage

### IT's OK to sell software too

* Carbon Copy Cloner
* Two canoes Complete Bootcamp Backup
* [Apple education IT MDM providers](https://www.apple.com/education/it)

## _What i'm seeing in the field..._

* No Ethernet
* No servers
* No directory
* DEP, MDM, VPP
  - DEP to enrol into MDM
  - MDM ot manage
  - VPP to distribute apps and book
* Pre-release information
  - keynote from WWDC

## Apple Support Articles

Check Arek's slides, read and weep

### Deployment references

* [MacOS Deployment Reference](https://help.apple.com/deployment/macos) (Safari
   and iBooks; and for iOS)
  - APFiS (Hooray)
    - WWDC session 715
    - `diskutil` and `asr` man page (requires understanding of what is a "valid
      system")
    - Use supported workflows
    - If you target the volume traditionally, things go bad. If you target the
      _container_ the asr restore works
    - Image with 10.12.6 then upgrade to 10.13 as part of bootstrap process
     (if it's not on 10.13 yet)
  - Content caching: With High Sierra, it's in Sharing preferences (perkele)
    - Options: Advanced Options
  - TLS 1.2 (which is the current version)
  - AutoDMG can still work
  - User approved kernel extension loading (UKULELE, was "SKEL")
    - *System extension blocked* Goto SecPri to allow, requires no admin privs
  - MDM macOS payloads and restrictions
    - see appendices of macOS Deployment references
  - Moar Docs!
    - Apple Configurator 2 / Apple School Manager
      - Manually add device to DEP or ASM
        - non-DEP iOS devices, 30 day provisional status, mandatory supervision
          and enrolment, iOS11/tvOS11
        - Apple Configurator 2.5
      - Classroom help
    - Profile manager help ...
  - Profile manager... without Open Directory
    - ...without Active Directory
    - spin up one for kicking the tires
  - support.apple.com/guide/{{APPNAME}}
    - See System Image Utility help (which may well fail anyway)
    - `keychain-access` help; if login passwd ≠ keychain passwd, User silently
      gets a new keychain and the old one is moved aside (but still available)
    - Two keychains, login and local items
    - `directory-utility` help/guide; no longer uses NIS Yellow Pages
  - _Was this help page useful? Send feedback!_

### Server 5.4

- File Server has moved to System preferences
- Certificates
- Profile Manager without OD/AD .. will create a PKI
- Profile Manager usually has a problem with OD when there are many clients;
  Now PM doesn't require OD
- Time Machine Service is also gone, moved to Sharing preferences
  - set up a shared Time Machine backup folder (HFS+)
  - SMB not AFP
  - Advanced: Share as a Time Machine backup location

## What can you manage on one platform but not the other (macOS/iOS)?

* Secure Boot to macOS, plx
* Activation Lock, plx
* Managed "Open In", plx, to prevent inadvertent leaking of information

### Finally a burning question

Should people use an administrator account for everyday use?
- For home users it's probably fine
- But what about in a company?

Admin vs standard
* Anyone can agree to a Kext now
* SIP prevents even root from modifying some things
* Adm required to enrol in and remove MDM
* Admin required to add printer and change date and time

If your users use standard accounts, you should too. In the end,
it's all about the users.
