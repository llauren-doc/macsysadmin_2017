Day 3, set 1

# Marko Jung: Security

Okay, so i missed the first half :/ (zzz)

## Compliance

Systems report compliance violations to your SIEM solution:
* Endpoint Security (osquery, santa, zentral)
* Server auditing (OpenSCAP, Lynis)
* Business application auditing (financial transactions)
* Automated vulnerability scans (Nessus)

We should probably do this ourselves.

## The tool maketh the team

*SIEM* (Elastic stack)
* gets
 - logs and Events
 - traffic analysis
 - complience
* begets
 - data aggregation
 - correlation, alerting, retention
 - forensic analysis
 - reporting

*SIRT* (Best Practical RT! https://bestpractical.com/ritr)
* gets/provides
 - Incident reports
 - Investigation
 - Countermeasures
* begets
 - priorisation
 - workflows
 - automation ...-

# Noteworthy thing

Oxford University is a whole ecosystem. It's an ISP. No real perimeter firewall.
All services are deployed to the Internet.

## Beyond Corp

Your network is untrusted and untrustable. Check the Google publication on this.

Check the Stanford University Password policy; the shorter your Password is,
the more complex it needs to be.

## Phishing

* Inform your admin
* Response Policy Zone (DNS)

| Procuct | Hit rate    |
| :------------- | :------------- |
| Sentinel One       | Item Two       |
| Sophos       | Item Two       |
| Carbon Black       | Item Two       |
| What       | Item Two       |
| CySomething      | Item Two       |

# Measure up

* Jeff Bollinger, Brandon Enright, Matthew
* [slides](https://github.com/mjung/publications)
