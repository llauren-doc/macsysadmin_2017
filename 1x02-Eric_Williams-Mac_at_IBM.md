Day 1, set 2

# Eric Williams: Mac at IBM

* Loads of devices (315k mobile under management)
* 170 TB of data
* 170 countries
* 9000 printers
* 200k mobile devices
* 600k laptops

### Mac at IBM is really a change of culture

* Before: get crappy Lenovos. Now: Get proper Macbook Pros.
* Update tooling
* Push self service model

## Biz case

Investment
- License and unfrastructure
- Mac help desk staff
- New Intranet support portal
- Project  staff (OCM)

Hard benefits, Soft benefits

- Zero touch deployment by DEP
- No instructions sent with Mac
- Emulate a consumer-like experience
  - Email "Your Mac is on the way"
  - "Say hello to your new Mac"
  - "We're here to help"
  - "Meet your new Mac"

## KPI

* 115k Macs, 200k iOS deployed since june 2015
* 64 help agents, 1:5000 <gent:device ratio
* 99.1% first time call resolution

IBM ThinkDesk walk-up service and appointments
- extra hw
- transfer data
- support staff

Watson powered iOS help desk app!

## What about legacy app portfolio?

- Mobile and web
- VDI
- Virtual machine

Of 3000 apps, 78 were problematic. Some apps were re-written to work with a Mac.

Employee engagement up ten points in Employee engagement, prime driver was
"Better tools"

## Success factors and lessons learned

- Start with *why*. The WHY guides the entire program
* Bring the security and finance people with you (early)
* Tooling matters: you need to make Mac-specific software purchase decisions
* People matter: If you want to move quickly, hire great people _who understand
  Macs_ - you don't have the time to grow this skill
* Focus on the _total experience_, not just the Macs directly (what are all
  the system an employee touches)
* No corporate macOS image!
* Don't block OS updates -- Day 1 support
* Help desk: Critical to hire people who are as passionate about the Mac as
  those who will be using Macs
* Help desk: focus on first call resolution
* Protect the native Mac experience (local admin rights, Filevault 2, XProtect)
* Enlist your existing Mac users as advocates, volunteers and helpers
* Application portfolio less problematic than anticipated
* Make it about _employee choice_, not a  forced march. By allowing employee
  choice at time of laptop refresh, this became a self-selecting group.
* Find a reasonable balance between risk and productivity
* Set aspirational goals (ie 50k Macs in 6 months)
* Incremental purchase price of a Mac is more than offset by reduced support
  burden

## Now what?

* Grow the Mac@IBM program
* Improve iOS mobile experience
* Apply learnings to PC@IBM
* Re-invest savings in Help Desk technologies
