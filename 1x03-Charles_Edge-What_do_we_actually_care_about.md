Day 1, set 3

# Charles Edge: What do we really care about?

* _We need a good reason to complain about_
* Whatever my boss says
* Making our deployments better
* The next job
* Fighting for the users
* Driving the community forward

## The Full Stack Mac Admin

- ...does it all
- writes middleware
- troubleshoot printer drivers (and binary drivers)
- release manager
- sends packages to devices, sometimes packages them
- speak with infosec teams (which isn't the same as security teams)
- common in startups

* Endemic to Mac infrastructure

- As dev teams grow, we specialize

Back in the day, all admins were full stack, because there weren't too
many of us.

* And it took so long to become an expert. And the deployments weren't big enough.
* We cared about servers. Podcast Producer. Open Directory...
* The servers that were always underpowered.
* Mobile home directories...
* The Dock
* Binding to Active Directory
* MCX
* XSan...

## So let's take a look

- Backup
- Directory services
- Scripting
- Packaging
- More and more security
- Imaging
- Less Server
- More device management
... ()

The trends are consistent.

## What do we care about now?

* Is imaging dead?
* Not macOS Server
* Scripting everything
* MDM
* Caching Server
* Providing an _amazing_ user service
* Keeping our systems Secure
* Managing the state of systems
* ITSM IT Service management
* Automating the request for software
* Viewing device details while creating a service ticket
* Locking a device from a service desk portal

## Why?

* Deployments are growing
* Is this the end of the Full Stack Admin?

## Where will we gravitate next?

* Platform engineers
* QA
* Release management, package builds
* Managing teams
* Infosec
* Ensuring the security of our Deployment
* Server management
* Middleware scripts
* Connecting systems to each other
* Log aggregation and analysis
* Other patch management systems
* HR Systems
* Identity management
* Line of business apps
* Self-healing systems
* ...and way way more

## What does all of this mean?

* Crossing platforms
* Is the Mac a mobile or desktop platform device?
* The impact of Scripting: knowing this is never a bad thing
* SKEL/UKULELE/Something Kernel Extensions Loading
* Disable SIP to `dtrace` ☞ Jailbreak
* Will all services need to be signed? Soon?
* Writing apps?

## How does SIP impact device state?

* You can't really edit the `/System`, so why monitor it? :)
* Even the smallest security incident ...
* Less and less access makes the deployment easier, but less powerful

## 10 years ago

* "iOS and the Mac are coming together"

* Deploy settings (with profiles)
* Deploy App store apps
* Deploy NON-App store apps
* Deploying settings into (running) apps
* Deliver a system in a known state?

## The concept of a filesystem

* "Entitlement"
* What if Mac didn't have a Finder?
* How do apps share data? Through Entitlements


## Who builds the tools of the future?

Those with the domain knowledge (of Macs)

## Mastering iOS management

...you can probably transfer some of this to macOS eventually
