Day 2, set 3

Duncan McCracken

# 50/50

_"Things i learned and was painful to find out"_

This is a brain dump of my daily life, in no particular order

## Have external interests

Don't make this your life.

// Don't fight the machine.

## Use a Single Source of Truth

## Store all your original data prior to manipulation

You might have made a mess of something when manipulating it and then
you have to go and recreate it. Keep a copy of the original stuff.

## When things go wrong, Don't Panic!

Take a step back and don't worry. Yet.

## Be methodical

...in your approach to _everything_ . Have tests, scripts, check lists.
Check in your code.

## Document your actions

Document your builds, your changes. Keep a record with you stuff. You can
refer back to what you've done.

When you change and change back, it's not a no-change, it's two changes.

## Know when to call it

You become ineffective when you get tired. You stop thinking clearly.
The first time i decided to ignore the demo gods...

Banging your head against the problem may probably just make it worse.

## Keyboard shortcuts are your friend

Touch bars are not keyboard shortcuts, even for the stupid.

## (Where possible,) use what's already there

Don't download yet another library or binary if you can avoid it.

## Learn a high-level languish

I once had a deadline in 48 hours and almost needed to learn Objective-C...

Scripts are slowly going away because they will need to be code signed..

## There is no undo in the CLI

```bash
# ^V + TAB is magical
time somescript.sh
basename ~/Desktop/foo.txt # returns foo.txt
dirname # is the opposite
file # tells you what something _is_
# a xar archive is actually a .dmg
# before you _do_ something to something, make a hard link
ln "/Applications/Install macOS High Sierra.app/.../foo.dmg" ~/work/foo.pkg
pkgutil --expand # and --flatten
open -a /path/to/something
strings # nice to inspect binaries for hidden options and fun
pkgutil --expand-full and --flatten-full
# Leverage undocumented features carefully
security cms -D -i foo.mobileconfig -o foo.plist # remove signature and ascii
pmutil ... # pretty-print
security cms -S -N "Developer ID Application: foo" ... # sign it but keep it ascii
profile -I -F foo.mobileconfig #install a Profile
```

remove: `/private/var/db/ConfigurationProfiles/Setup/.profileSetupDone` and
replace with `/private/var/db/ConfigurationProfiles/Setup/SomeSignedProfile`
and you have a first boot profile!

## Brace your Bash variables

```bash
var=foo-bar
echo ${var}foo      # foo-bar-foo
echo ${var/bar/foo} # foo-foo
```

## Finding stuff using sporligt

```bash
% mdfind kMDItemCFBundleIdentifier = "org.mozilla.firefox"
/Applications/Firefox.app
% find /Applications -iname firefox.app #will take forever
```

## Find Desktop by User

```bash
find ~/Desktop -user root
find ~/Desktop -group admin
find ~/Desktop -newerct '15 minutes ago'
find ~/Desktop -name .DS_Store -delete # be careful
find ~/Desktop -name *.py -exec open {} \;
```

## Bash IFS

This is useful on finding files with spaces

```bash
IFS=$'\n'
# unset IFS
```

## Redirection

```bash
xcode-select | grep install # fails if xcode not installed
xcode-select 2>&1 | grep install # works even if xcode not instlled
```

## XML manipulation from the command line

`xpath` is your friend. Learn it.

```
% xpath foo.xml "//InstallInfo/PackageName/text()" 2>/dev/null
% echo $(xpath foo.xml "//InstallInfo/PackageName/text()" 2>/dev/null)
```

Add in `for`

`for i in $(seq $(xpath something.xml "count ...something"))` uh

## Writing nested plist keys

Just write xml to defaults

`defaults write foo.plist Node "<dict><key>boolean</key><true/><key>array<key>...`

## Finding Java

`java` -- shows a dialogue box

`/usr/libexec/java_home 2>/dev/null ; echo $?` fails nicely if there is no java installed.

## Misc

`^X + ^E` will open your command line in `$EDITOR`
