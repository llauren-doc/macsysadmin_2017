Day 3, set 4

# Greg Neagle - Munki 3

## Woz' new?

* Import `macOS Installer.app`
* Supports authenticated restarts (if FileVault wants creds)
* Repo Plugins, originally with Centrify, to use other cloud providers for your repo
* Commercial vendors ❤️ Munki, next up Airwatch
* OOBE for Featured Items, so the first time user won't see a bucketload of Optional installs
* DANGER: Allow Untrusted install
* Autoremove unused really expensive software

## Munki 3.1

* Support macOS High Sierra (supports system/software updates)
* Enhanced bootstrapping: run Munki (once) immediately after OS update is complete
* MSC shows software that needs OS updates to be installed or updated

## Munki 3.x

We make it up as it goes along
