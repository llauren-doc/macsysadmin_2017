Day 3, set 2

# Sam Keeley - Securing the Sysadmins (Practical Security for the Mac Sysadmin (brief encounter))

_"Things i wish someone had made me think about long ago"_

*  Everyone should have an ownership of security.

## Ponder this

* What access does a "standard user" have in your organisation?
* What access do _you_ have as a sysadmin?
  - elevated user with elevated responsibilities and ditto risks
  - you are probably more important to your organisation than you think
  - You are the target, defend yourself

## Access is key
  * Sysadmins have especially useful access, useful to amplify or zone in attacks
  * Nobody is going to be perfectly safe, but thinking security for the long term is kinda key
  * An attacker with _unlimited_ resources can certainly acheive their goals, but reality often imposes limits (you only need to be the second fastest runner)
  * Make it so expensive in time or effort that they go somewhere else (and the attackers don't win)

## A little game

Never have i ever...
* Connected to an RDP/VNC session from another user's computer
* ... and left the credentials saved?
* Using my own credentials for an application such as JSS or printer's LDAP lookups
* Used the same password for multiple Applications
* Submitted my password directly to an application (like a login form)

All of these actions can lead to the loss of your credentials. In a perfect world, you would only submit your password from which it originated.  You don't put your Google password to a nongoogle form. Do you trust your services?

## Passwords vs keys

Passwords
* hopefully memorable
* commonly short (64-128 bits)
* password itself is the secret
* Submit the secret to the requestor
* Easily phishable
* Crackable
* No ability to verify _single possession_ (you can't verify how many services have your password)

Keys
* Not memorable
* Long (1024+ bits)
* Private (not public) key is the secret
* Submit proof of private key control to requestor
* Hard/near-impossible to phish
* Effectively not crackable (2048+ bits)
* Can be secured easily to keep non-stealable
* Can't really be intercepted to services (except ssh agent forwarding through an untrusted server)

Thus: Passwords are bad :D

* Should not be the keys to the kingdom
* You should not know all your passwords (use a password manager)

Keys are good!

* ssh (including git remote)

## Key security

Non-exportable keys, like Yubikeys (hard token, PKCS#11), can be physically but not electronically stolen.

## No mo' yolo

The compromise of _you_ can lead to a lot of bad things. How many types of actions can _you_ take alone, without review or checks? It takes two keys to launch a missile. What's your organisation's missile?

## Some tools (TO ØL S)

* on JAMF - it's on the bloody Internet - and it has a _very_ nice API...
  - disabling the web application doesn't disable the API (you may want to restrict it)
  - Configure SAML based SSO using a secure provider using MFA (OneLogin, Duo, Okta, Google cloud identity)
  - Make changes through API on test server, the deploy to public
* Everything else
  - Munki, Puppet, Imagr, DeployStudio, Chef, Salt...
  - Can be controlled solely through text files, making version control through git easily
  - enables _enforced_ code review (eg to merge to master branch)
  - used in conjunction with github(gitlab) or Phabricator, be sure that changes require at least two to act
  - Ensure that master pushes are blocked, merges myst happen online

## Please mind the Security
