Day 3, set 4

# Tim Standing OWC - Dive into APFiS

Follow-up from MacSysadmin 2016
* Study by Google of 100,000 disks over 8 months (2007)
  - checked SMART attributes every 24 hours for every sidk in their server pods
  - noticed correlation between certain SMART attributes and future disk failure

SMART Alec will check your disk SMART attributes (no cost).

* SMART Alec icon face should change with status :D
* https://smartalec.biz
* Also anon failure stats gathering

## History

* 1998 = HFS+ and the 1st iMac _("Sorry, no beige")_

Attempts to replace HFS+
* 2006: ZFS (is great, as long as you have ECC RAM, or the system goes down in about a year... Also requires *LOTS* of RAM)
* 2007-2011: two other rumoured, cancelled projects
* 2011: Core Storage (magic shim layer over HFS+) to add features without changing HFS+

## Why would Apple create a new file system?

* 64 bit file system; more files, smaller chink size on large volumes
* Tuned for SSDs
* Space sharing, dynamic resizing for volumes on the same disk
* Increased protection for volume metadata
* Reserve size and volume quotas
* Copy on Write-snapshots; more efficient storage of different file versions
* Low latency file operations
* More robust encryption
* A NEW SOURCE CODE BASE

## And what would we want?

* Increased protection from corrupted volumes
* Volume snapshots
* Increased speed on HDD
* More robust encryption

Copy-on-write is nice; if on HFS+ your power goes when writing, shit happens. New metadata is protected by checksum and is not overwritten before it's really written.

Reserve size and quotas

`diskutil apfs list diskX`

## How does Copy on write work?

* Initially, it's like hard links
* Edit the copy, creates diffs for both original and copy, changes removed from the original
* So don't use APFS on rotating media (currently)

## APFS snapshots

* Instantaneous picture of all the files on an APFiS volume
* Stored in the same container as the APFiS volume
* Created in 1-2 seconds
* Thakes up very little space as they using

### Creating an APFiS snapshot
```
tmutil localsnapshot /
tmutil listlocalsnapshots /
tmutil deletelocalsnapshot / # and reboot
```

## APFS encrypted volumes

* Integrated into the file system
* Supports encrypted Startup volumes

```
sudo duskutil apfs list disk8
sudo diskutil apfs encryptvolume disk8s1 -user disk
```

## Supporting APFS volumes

* Do your backup utilities work?
* Can you recover corrupted volumes?
* Can data Recovery companies get files from failed disks?

## Recommendations

* Don't use APFiS on HDD -- ever
* Don't encrypt or decrypt a volume larger than 200 GB (20 hours to complete)
* Only APFS for volumes that are ACTIVELY backed up
* Use snapshots as a precaution before every software install or system update
* Expect non-Time Machine machups and file copies to take twice as long as HFS+
* Expect APFS to be required for Startup volumes in macOS 10.14

_"Thunderbolt is like VPN for PCI"_
