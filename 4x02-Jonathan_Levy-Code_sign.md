Day 4, set 2

# Jonathan Levy: Signed, sealed and blah blah blah

aap

http://newosxbook.com/tools/jtool

```bash
sudo #!/usr/bin/env bash
sysctl vm.cs_enforcement=1
```

...enforces code signatures (though only the TEXT segment)

% ./jtool --ent /bin/ps

# Part II on auditing

| tool    | examples |
| :------ | :------- |
| dtrace  |          |
| kdebug  |          |
|         |          |
|         |          |
|         |          |
|         |          |

```
/usr/bin/fs_usage
