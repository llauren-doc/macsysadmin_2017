Day 2, set 1

Armin Briegel

# #!/bin/bash

Scripting is documentation. At least _some_ level of documentation. Not necessarily good documentation.

*Biffen börjar här*

Defensive coding
```bash
buildFolder="${1:?'Argument missing, bailing out!'}"
buildFolder="${1:-'Default value'}"
```

Use `shellcheck.net`

## Strict mode

Fail fast

```bash
set -euo pipefail

set -e          # fail on non-zero Command
set -u          # fail on unset variables
set -o pipefail # any element of a pipe chain fails
```

Trace mode

```bash
set -x          # aka bash -x script.sh
set +x          # disable set -x :D
```

## `bash` vs everything else

Bash Pros
* is everywhere
* process control, file management
* tools like akw, sed, grep, networksetup...
* lots of documentation and samples and help and knowledge
* if it's on the macadmins Slack, it's probably already bash

Bash cons
* fairly simple data types, dict, array (bash 4), not plist, xml, json
* user interaction isn't too great

Others
* python
* AppleScript
* Swift

AppleScript
* Inter application data communication (where supported)
  - FileMaker, Adobe*, MS Office, etc
  - Simple UI: notifications, alerts...
* Usually requires logged in user
* Troublesome when running as `root`
* Future?

`bash` + AppleScript
```bash
% osascript -e 'display alert "Hello, World!"'

function embedapplescript () {
  osascript << EOS
    tell application "Finder"
    ...
EOS
}
```

Python
* comparatively easy to learn
* Python - Cocoa/Objective-C bridge

```python
from FOundation import CFPreferenceCopyAppValue
print CFPreferenceCopyAppValue("idleTime", "com.apple.screensaver")
```

Swift
* Compiled or interpreted, and Playgrounds
* "Native" languate to macOS
* Complex data types
* Native apps
* Can be complex OO language
* moving target (changes between versions)
* fairly steep learning curve

## more

`scriptingoxs.com/bash`
