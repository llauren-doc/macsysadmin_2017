Day 2, set 5

# Jesse Peterson: Open Source MDM

# Part I: MDM + DEP, protocol, spec et cetera

The MDM spec was behind a paywall before.

### Why?

What does MDM on Macs do for me?
* Install profiles
* Install packages
  - which we can do with better tools
* DEP bootstrapping
* Remote wipe(-ish)
* VPP app distribution
* SKEL/UAKEL

iOS has Supervised Mode. macOS has not. This is subject to change.

## DEP is...

* deploy.apple.com
* DEP API
  - often json
  - OAuth authentication
  - MDM _something_ syncs with _something else_
* Device fetches a profile

## MDM protocol and specification

## MDM on the wire

- just an HTTPS server
- most HTTPS bodies are XML PLISTs
- Sometimes responses are wrapped in encrypted or signed DMS (PKCS#7) messages, or have detached signatures
- APNs or APNs HTTP/2 (but not yet JWT APNs auth)
- SCEP protocol also HTTP

## Enrolment

```
https://mdm.my.org/\
    /enroll
    /scep
    /ota/enroll
```

## Checkin

```
https://mdm.my.org/\
    /checkin
```

* Authenticate to the MDM server (signed, cert, stuff).
* Push magic

...and so you are enrolled!

## Command queue

```
https://mdm.my.org/\
    /mdm/connect
```

Requests the next queue command, then request the next one, until queue is empty ("exhausted").

First you send a push notification to the device, only then the device checks in with the server.

## Command: InstallApplication

Install any signed, valid, flat distribution .pkg packages using MDM. Often used simply to install the vendor's agent. Duh.

For it to work, you need a Manifest (and some metadata), which lies on a *non-authenticated* http or https server.

## DEP(boot)strap process

* Power on
* Fetch DEP profile (if there's a network connection) from Apple.  Doesn't use it yet though.
* MDM applied on OOBE
  - user account creation (manual or MDM)
  - per-user MDM token update -- MDM can target single device or user if necessary
* DEP enrolment done

## To help you
* github.com/erikng/installapplications
* DEPNotify gitlab.com/Mactroll/DEPNotify
* *Virtualize* your DEP testing https://goo.gl/XuLWYB @rderwianko

## The bigger picture: DEP + MDM

## MDM: The bad part

* APNs and DEP network requirement
* APNs and DEP availability, reliability, scalability
* Lack of adequate _desired state_ inspection
* Complexity and restrictions of setting up an MDM
* MDM spec itself is terrible and vague in many areas
* MDM spec/features are glacial...

## The bugs

* `mdmclient` terribly documented and buggy (Apple doesn't follow its own spec)
* `storedownloadd` extremely fragile
* ever-evolving DEP `InstallApplication` context
* `ScheduleOSUpdate` and friends utterly unreliable

# Part II: MicroMDM

## Why?

* Curiosity
* Commercial MDM isn't doing what it's needing to do
* Extensibility, expandability, API, hooks
* Source is ... open!
* Community support
* Not just a pretty GUI/Web ui
* Cost

https://micromdm.io

## Up and running

* Get a push notification Certificate
* For testing, export Certificate from `Server.app` from Keychain Access :D
* https://mdmcert.download
* Apple says **MDM IS NOT FOR INDIVIDUALS** -- which is silly.
* Create a new push Certificate (which is not a CSR) at identity.apple.com, assumedly
* Signing up for DEP: You need a DUNS number
* A NEW Apple ID, call Apple, buy devices through vendor that supports DEP registration (not through Apple Stores)
* You do not need a SSL certificate for the microMDM server

Read micromdm.io blog

# Video demo

Note, the server URL cannot change during the life of a device.
