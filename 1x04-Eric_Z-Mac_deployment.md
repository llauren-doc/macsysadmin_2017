Day 1, set 4 (ez@apple.com)

# Eric Z: Mac Deployment in the new world

Apple: We're not abandoning the (non-book) Mac. New desktops _are_ coming.

WWDC17: iMac Pro: the most expensive Mac _ever_ :D . Will ship in December.

High Sierra sets up for future hardware.

## Deploying High Sierra

* Installing
* Firmware
* File systems
* Deployment paths
* Secure Kernel Extensions

Multiple installers. 20 GB installer for El Cap and Sierra. 20 MB installer for Yosemite eand earlier. Blocked network access; swscan.apple.com and swdonwload.apple.com are blocked or if your network network access is blocked... :/ or you have custom software update catalogs.

*Requires Internet connection*

Updates Firmware; the Installer is the only Privileged Thing to update the Firmware.
- Runs as RAM disk
- Privileged process

Target disk mode is _not_ a supported install method

## Firmware

* `810-54-2136-1 RM-NCE-1`
* Pepin and DUO labs found that machines were updated but firmware was not. This was probably because they were installed by imaging, not the Installer.
* Goal: Make a Mac as secure as an iOS device.
  - Firmware needs to be Latest
  - No Nation States etc shall put their firmware on your system
* Required Firmware Updates: SSD, EFI, SMC, USB-C, EmbeddedOS, etc
* Device specific: Download direct from Apple
* Firmware Security: EFI is validated weekly

## File systems

Apple File System
- APFiS, APFiS Encrypted, APFiS Case Sensitive, APFiS Case Sensitive Encrypted
- Default boot file system on 10.13.0 and +
- APFiS on Flash and SSD (whut?)
- CoreStorage/HFS+: Fusion drive and HDD
- APFiS formatted drives can be r/w on macOS Sierra 10.12.6 (non-encrypted volumes only)
- APFiS formatted volumes are not backward compatible
- Volume encryption in Finder auto-vonverts HFS+ to APFiS

### Boot camp?
- Cannot r/w APFiS volumes
- Use Startup manager for now
- 3 TB Fusion drive issue

### File Sharing
- SMB is preferred file sharing protocol for client and server
  - Search over SMB still needs some love
- APFS formatted volumes _cannot_ be shared over AFP
- Use SMB or NFS instead

### Time machine
- uses HFS+
- Local (need to format as HFS+) and Network backups

*Update your backup and disk repair tools*

### Transition Roadmap

* Was: released for iOS, tvOS and watchOS; boot file system
* Is: APFiS boot vol conversion during install for SSD/FLASH
* Will be: working

## Methods to install or upgrade
1. Use the macOS installer
2. Create a bootable installer
3. Internet restore
4. Uh, something

`Alt Command R` _will_ install High Sierra

System imaging: _"Apple doesn't recommend or support monolithic system imaging when upgrading or updating macOS."_ -Apple

*Reminder* Installer requires Internet connection, Target Disk Mode is not supported.

WHY can't i do monolithic imaging?

_"More than bits in the file system are needed to operate a Mac"_ -Apple
(like the EFI, Secure enclave, future HW)

"Imaging methods with bypass the macOS Installer are not preferred as the firmware updates will be missing from the installation. This will cause the Mac to operate in an unsupported and unusable state." -Apple

## BUT!

You can image a Mac that is on High Sierra, because it now has all the firmware updates. You can use it for backup and restore:
```
sudo asr restore ...
```

Understand what a Valid APFiS system is.

Summary. System Imaging...
- Works with current Mac hardware
- Firmware not updated
- Hardware specific on-demand resources not installed
- May require additional downloads and restarts
- *Will break* with iMac Pro and future hardware

Installer: Good. Imaging: Neeh.

## Hernel Extensions

- requires _user_ approval for _newly installed_ Kernel Extensions (system upgrade: kexts are grandfathered in)
- Why? Security. Mac as secure as iOS.
- Disable in Recovery mode/drive (like SIP)
- Resets with NVRAM: use Firmware Password to prevent NVRAM changes
- In edu, gov, ent, this is Not Good User Experience. This feature was installed after the Developer Conference, it was slipped in and we've lived with the ramifications since...
- To disable, enrol into MDM
- MDM configuration of UAKEL available in a future update
- not required to update an existing KEXT

```
spctl kext-concent status ...
```

Apple: Beta-testing is a year-around effort

_"You should be testing 10.13.1 right now, especially if you're using Active Directory"_

TIP: Look into NBase-T, for Fun (and profit)
