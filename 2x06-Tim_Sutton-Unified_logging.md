Day 2, set 6

# Tim Sutton - Unified Logging for Sysadmins

```
% syslog -w

NOTE:  Most system logs have moved to a new logging system.
See log(1) for more information.
```

See also: Penn State MacAdmins 2017: LOG! Peeling back the bark,
by @chilcote

https://developer.apple.com/videos/play/wwdc2016/721

## How UL differs

Efficiency
* Logs to a proprietary database (only `log` and Console can
  read this, others can't read this db)
* Persistence of log data varies depending on importance (!)
* Message formatting happens at display / review time, not at writing

Log message types and scopes
* Standard: Default, Info, Debug
* Special: Error, Fault (more for process and activity information) are stored
  in a special database
* Events can belong to a subsystem, and categories within

Privacy
* Log messages scrubs out passwords or certain user accounts

Show vs Stream
* Stream is realtime but more resource intensive
* Console app opens stream

## Configuration

* eg `sudo log config --mode 'level:debug' --sunsystem com.apple.WenDriver`

```zsh
% ls /System/Library/Preferences/Logging/Subsystems
% ls /System/Library/Preferences/Logging/resources
% ls /Library/Preferences/Logging/Subsystems
```

## `/usr/bin/log`

`log` shows usage, but the options don't do anything. The `man` page is
more accurate.

Check the demo from the video later

`ecletcitlight.co/category/macs` (Howard Oakley) - Consolation, and
MakeLogArchive

* No public API for reading logs, but this may change.
* Some stuff is forwarded from old system to the new, but not (maybe)
  `syslog` actual :/

[Moar info](https://macops.ca/logs)
